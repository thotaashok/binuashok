Pod::Spec.new do |spec|

  spec.name         = "binuashok"
  spec.version      = "0.0.1"
  spec.summary      = "Summary of pod "

  spec.description  = <<-DESC
This CocoaPods library helps you perform Binu Proxy.
                   DESC

  spec.homepage     = "https://thotaashok@bitbucket.org/thotaashok/binuashok.git"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "thotaashok" => "thota.ashok@gmail.com" }

  spec.ios.deployment_target = "12.0"
  spec.swift_version = "5.0"

  spec.source        = { :git => "https://bitbucket.org/thotaashok/binuashok/src/master/", :tag => "#{spec.version}" }
  spec.source_files  = "Binu/*.{h,m,swift}"
  spec.dependency 'Alamofire'
  spec.dependency 'MatomoTracker'

end
